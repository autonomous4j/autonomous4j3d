/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autonomous4j3d;

import static autonomous4j3d.FlightCommand.mapToFlightCommand;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.Collectors.toList;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.fxyz.cameras.CameraTransformer;
import org.fxyz.extras.CubeWorld;


/**
 *
 * @author Sean
 */
public class Autonomous4j3D extends Application {

    private PerspectiveCamera camera;
    private final double sceneWidth = 600;
    private final double sceneHeight = 600;
    private double cameraDistance = 1000;
    private double scenex, sceney, scenez = 0;
    private double fixedXAngle, fixedYAngle, fixedZAngle = 0;
    private final DoubleProperty angleX = new SimpleDoubleProperty(0);
    private final DoubleProperty angleY = new SimpleDoubleProperty(0);
    private final DoubleProperty angleZ = new SimpleDoubleProperty(0);
    private CameraTransformer cameraTransform = new CameraTransformer();
    private CubeWorld cubeWorld;    
    private Group sceneRoot = new Group();    
    
    private double mousePosX;
    private double mousePosY;
    private double mouseOldX;
    private double mouseOldY;
    private double mouseDeltaX;
    private double mouseDeltaY;    
 
    @Override
    public void start(Stage primaryStage) {

        Scene scene = new Scene(sceneRoot, sceneWidth, sceneHeight, true, SceneAntialiasing.BALANCED);
        scene.setFill(Color.BLACK);
        //setup camera transform for rotational support

        camera = new PerspectiveCamera(true);
        camera.setNearClip(0.1);
        camera.setFarClip(20000.0);
        camera.setTranslateZ(-1000);
        scene.setCamera(camera);
        cubeWorld = new CubeWorld(1000, 100, true);
        cubeWorld.getChildren().add(cameraTransform);
        cameraTransform.setTranslate(0, 0, 0);
        cameraTransform.getChildren().add(camera);
        cameraTransform.ry.setAngle(-45.0);
        cameraTransform.rx.setAngle(-10.0);
        
        sceneRoot.getChildren().addAll(cubeWorld);
        
        //add a Point Light for better viewing of the grid coordinate system
        PointLight light = new PointLight(Color.WHITE);
        cameraTransform.getChildren().add(light);
        light.setTranslateX(camera.getTranslateX());
        light.setTranslateY(camera.getTranslateY());
        light.setTranslateZ(camera.getTranslateZ()+500.0);        
        scene.setCamera(camera);

        //First person shooter keyboard movement 
        scene.setOnKeyPressed(event -> {
            double change = 10.0;
            //Add shift modifier to simulate "Running Speed"
            if (event.isShiftDown()) {
                change = 50.0;
            }
            //What key did the user press?
            KeyCode keycode = event.getCode();
            //Step 2c: Add Zoom controls
            if (keycode == KeyCode.W) {
                camera.setTranslateZ(camera.getTranslateZ() + change);
            }
            if (keycode == KeyCode.S) {
                camera.setTranslateZ(camera.getTranslateZ() - change);
            }
            //Step 2d:  Add Strafe controls
            if (keycode == KeyCode.A) {
                camera.setTranslateX(camera.getTranslateX() - change);
            }
            if (keycode == KeyCode.D) {
                camera.setTranslateX(camera.getTranslateX() + change);
            }
        });

        scene.setOnMousePressed((MouseEvent me) -> {
            mousePosX = me.getSceneX();
            mousePosY = me.getSceneY();
            mouseOldX = me.getSceneX();
            mouseOldY = me.getSceneY();
        });
        scene.setOnMouseDragged((MouseEvent me) -> {
            mouseOldX = mousePosX;
            mouseOldY = mousePosY;
            mousePosX = me.getSceneX();
            mousePosY = me.getSceneY();
            mouseDeltaX = (mousePosX - mouseOldX);
            mouseDeltaY = (mousePosY - mouseOldY);

            double modifier = 10.0;
            double modifierFactor = 0.1;

            if (me.isControlDown()) {
                modifier = 0.1;
            }
            if (me.isShiftDown()) {
                modifier = 50.0;
            }
            if (me.isPrimaryButtonDown()) {
                cameraTransform.ry.setAngle(((cameraTransform.ry.getAngle() + mouseDeltaX * modifierFactor * modifier * 2.0) % 360 + 540) % 360 - 180);  // +
                cameraTransform.rx.setAngle(((cameraTransform.rx.getAngle() - mouseDeltaY * modifierFactor * modifier * 2.0) % 360 + 540) % 360 - 180);  // -
                cubeWorld.adjustPanelsByPos(cameraTransform.rx.getAngle(), cameraTransform.ry.getAngle(), cameraTransform.rz.getAngle());
            } else if (me.isSecondaryButtonDown()) {
                double z = camera.getTranslateZ();
                double newZ = z + mouseDeltaX * modifierFactor * modifier;
                camera.setTranslateZ(newZ);
            } else if (me.isMiddleButtonDown()) {
                cameraTransform.t.setX(cameraTransform.t.getX() + mouseDeltaX * modifierFactor * modifier * 0.3);  // -
                cameraTransform.t.setY(cameraTransform.t.getY() + mouseDeltaY * modifierFactor * modifier * 0.3);  // -
            }
        });        
         
        primaryStage.setTitle("Autonomous4j3D Playback simulation");
        primaryStage.setScene(scene);
        primaryStage.show();
        //Set up an easy drag and drop for loading a play back file
        scene.setOnDragOver((DragEvent event) -> {
            Dragboard db = event.getDragboard();
            if (db.hasFiles()) {
                event.acceptTransferModes(TransferMode.COPY);
            } else {
                event.consume();
            }
        });
        
        // Dropping over surface
        scene.setOnDragDropped((DragEvent event) -> {
            Dragboard db = event.getDragboard();
            boolean success = false;
            if (db.hasFiles()) {
                success = true;
                String filePath = null;
                filePath = db.getFiles().get(0).getAbsolutePath();
                System.out.println(filePath);                
                playFile(db.getFiles().get(0));
            }
            event.setDropCompleted(success);
            event.consume();
        });        
    }

    private void playFile(File file) {
        if (file != null) {        
            Task <Void> task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    try {
                        //Read in the playback file:
        //                List<DroneData> playback = Files.lines(file.toPath())
        //                        .filter(line -> line.startsWith("a4j"))
        //                        .map(mapToDroneData)        
        //                        .collect(toList());
                        System.out.println("Parsing Data File...");
                        List<FlightCommand> playback = Files.lines(file.toPath())
                                .filter(line -> !line.isEmpty())
                                .map(mapToFlightCommand)        
                                .collect(toList());    
                        Overlord overlord = new Overlord();
                        overlord.getTransforms().add(new Scale(10, 10, 10)); 
                        Platform.runLater(() -> {
                            sceneRoot.getChildren().add(overlord);                                 
                        });
                        System.out.println("Rendering Flight Commands...");                        
                            playback.stream().forEach(flightCommand ->{
                                    System.out.println(flightCommand.command);                        
                                    switch(flightCommand.commandType) {
                                        case TAKEOFF : { overlord.takeoff(flightCommand.power, flightCommand.duration); break; }
                                        case HOVER : { overlord.hover(flightCommand.power, flightCommand.duration); break; }
                                        case FORWARD : { overlord.forward(flightCommand.power, flightCommand.duration); break; }
                                        case BACKWARD : { overlord.backward(flightCommand.power, flightCommand.duration); break; }
                                        case LEFT : { overlord.left(flightCommand.power, flightCommand.duration); break; }
                                        case RIGHT : { overlord.right(flightCommand.power, flightCommand.duration); break; }
                                        case LAND : { overlord.land(flightCommand.power, flightCommand.duration); break; }
                                    }
                                try {                                    
                                    Thread.sleep(flightCommand.duration);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(Autonomous4j3D.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            });
                    } catch (IOException ex) {
                        Logger.getLogger(Autonomous4j3D.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }        
            };
            Thread thread = new Thread(task);
            thread.setDaemon(true);
            thread.start();    
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}