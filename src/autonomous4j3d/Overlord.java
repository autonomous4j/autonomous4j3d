/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autonomous4j3d;

import com.interactivemesh.jfx.importer.ImportException;
import com.interactivemesh.jfx.importer.obj.ObjModelImporter;
import java.net.URL;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.shape.MeshView;
import javafx.scene.transform.Translate;
import javafx.util.Duration;

/**
 *
 * @author Birdasaur
 */
public class Overlord extends Group {

    public Boolean inFlight = false;
    public Double sensitivity = 10.0;
    
    public Overlord() {
        ObjModelImporter objImporter = new ObjModelImporter();
        try {
            URL modelUrl = this.getClass().getResource("tie1.obj");
            objImporter.read(modelUrl);
        } catch (ImportException e) {
            // handle exception in next years JavaOne
        }
        MeshView[] meshViews = objImporter.getImport();
        getChildren().addAll(meshViews);
        setOpacity(1.0);
    }
    public void takeoff(Integer power, Integer duration) {
//        Platform.runLater(() -> { setTranslateY(-power * duration); });
        //Can't really use a Path here because we are in 3D         
        final Timeline t = new Timeline();
        t.getKeyFrames().addAll(new KeyFrame[]{
            new KeyFrame(Duration.millis(duration), new KeyValue[]{// Frame End                
                new KeyValue(yTranslateProperty(), getyTranslate() + (-power*sensitivity), Interpolator.EASE_BOTH),
            })
        });
        t.playFromStart();        
    }
    public void hover(Integer power, Integer duration) {
        //setTranslateY(power * duration);
        System.out.println("hovering...");
        ScaleTransition scaleTransition = 
            new ScaleTransition(Duration.millis(duration/2), this);
        scaleTransition.setToX(1.1f);
        scaleTransition.setToY(1.1f);
        scaleTransition.setToZ(1.1f);
        
        scaleTransition.setCycleCount(2); //make sure to set transition time to half duration
        scaleTransition.setAutoReverse(true); 
        scaleTransition.play();
        
    }
    public void forward(Integer power, Integer duration) {
        //Platform.runLater(() -> { setTranslateZ(power * duration); });
        final Timeline t = new Timeline();
        t.getKeyFrames().addAll(new KeyFrame[]{
            new KeyFrame(Duration.millis(duration), new KeyValue[]{// Frame End                
                new KeyValue(zTranslateProperty(), getzTranslate() + (power * sensitivity), Interpolator.EASE_BOTH),
            })
        });
        t.playFromStart();        
    }
    public void backward(Integer power, Integer duration) {
        //Platform.runLater(() -> { setTranslateZ(-power * duration); });
        final Timeline t = new Timeline();
        t.getKeyFrames().addAll(new KeyFrame[]{
            new KeyFrame(Duration.millis(duration), new KeyValue[]{// Frame End                
                new KeyValue(zTranslateProperty(), getzTranslate() + (-power * sensitivity), Interpolator.EASE_BOTH),
            })
        });
        t.playFromStart();        
    }
    public void left(Integer power, Integer duration) {
        //Platform.runLater(() -> { setTranslateX(-power * duration); });
        final Timeline t = new Timeline();
        t.getKeyFrames().addAll(new KeyFrame[]{
            new KeyFrame(Duration.millis(duration), new KeyValue[]{// Frame End                
                new KeyValue(xTranslateProperty(), getxTranslate() + (-power * sensitivity), Interpolator.EASE_BOTH),
            })
        });
        t.playFromStart();        
        
    }
    public void right(Integer power, Integer duration) {
        //Platform.runLater(() -> { setTranslateX(power * duration); });
        final Timeline t = new Timeline();
        t.getKeyFrames().addAll(new KeyFrame[]{
            new KeyFrame(Duration.millis(duration), new KeyValue[]{// Frame End                
                new KeyValue(xTranslateProperty(), getxTranslate() + (power * sensitivity), Interpolator.EASE_BOTH),
            })
        });
        t.playFromStart();        
    }
    public void land(Integer power, Integer duration) {
        //Platform.runLater(() -> { setTranslateY(power * duration); });
        final Timeline t = new Timeline();
        double currentHeight = getyTranslate(); 
        t.getKeyFrames().addAll(new KeyFrame[]{
            new KeyFrame(Duration.millis(5000), new KeyValue[]{// Frame End                
                new KeyValue(yTranslateProperty(), -currentHeight, Interpolator.EASE_BOTH),
            })
        });
        t.playFromStart();        
    }
    
    private void updateGroup() {
        Translate translate = new Translate(getxTranslate(), getyTranslate(), getzTranslate());
        //getTransforms().add(translate);
        setTranslateX(getxTranslate());
        setTranslateY(getyTranslate());
        setTranslateZ(getzTranslate());
    }
    
    private final DoubleProperty xTranslate = new SimpleDoubleProperty(0) {
        @Override
        protected void invalidated() {
            updateGroup();
        }
    };

    public final double getxTranslate() {
        return xTranslate.get();
    }

    public void setxTranslate(double value) {
        xTranslate.set(value);
    }

    public DoubleProperty xTranslateProperty() {
        return xTranslate;
    }
    private final DoubleProperty yTranslate = new SimpleDoubleProperty(0) {
        @Override
        protected void invalidated() {
            updateGroup();
        }
    };

    public final double getyTranslate() {
        return yTranslate.get();
    }

    public void setyTranslate(double value) {
        yTranslate.set(value);
    }

    public DoubleProperty yTranslateProperty() {
        return yTranslate;
    }
    private final DoubleProperty zTranslate = new SimpleDoubleProperty(0) {
        @Override
        protected void invalidated() {
            updateGroup();
        }
    };

    public final double getzTranslate() {
        return zTranslate.get();
    }

    public void setzTranslate(double value) {
        zTranslate.set(value);
    }

    public DoubleProperty zTranslateProperty() {
        return zTranslate;
    }    
}