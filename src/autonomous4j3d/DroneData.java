/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autonomous4j3d;

import java.util.function.Function;

/**
 *
 * @author Sean
 */
public class DroneData {
    String type; //Flight, Nav etc
    String name; //movement, yaw, speedy, speedx, etc
    String value; //either a decimal value or a movement command,number
    //examples:  a4jnavdata/yaw -179.218 or a4jflight/movement BACKWARD,20
    
    public DroneData(String line) {
        String[] lineTokens = line.trim().split("\\s+"); //break up into major tokens
        //First token is the type and name split by a "/"
        String[] typeAndName = lineTokens[0].split("/");
        type = typeAndName[0];
        name = typeAndName[1];
        //Second major token is the value
        value = lineTokens[1];
    }
    
    public static Function<String, DroneData> mapToDroneData = (line) -> {
        return new DroneData(line);
    };    
    
}
