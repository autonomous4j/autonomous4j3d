/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autonomous4j3d;

import java.util.function.Function;

/**
 *
 * @author Sean
 */
public class FlightCommand {
    
    public static enum COMMAND_TYPE {
        TAKEOFF,
        HOVER,
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT,
        LAND
    };
    
    String command; //incoming string from feed
    COMMAND_TYPE commandType; //TAKEOFF, FORWARD, HOVER, BACKWARD, LEFT, RIGHT, LAND
    Integer power; //0-100
    Integer duration; //milliseconds
    
    public FlightCommand(String line) {
        //Trim off the whitespace, curlies and split on delimiter
        String cmdStr = line.trim();
        cmdStr = cmdStr.replaceAll("\\{", "");
        cmdStr = cmdStr.replaceAll("\\}","");
        String[] lineTokens = cmdStr.split(",");
        if(lineTokens.length >= 2) {
            command = lineTokens[0];
            switch(command) {
                case "TAKEOFF": { commandType = COMMAND_TYPE.TAKEOFF; break; }
                case "HOVER": { commandType = COMMAND_TYPE.HOVER; break; }
                case "FORWARD": { commandType = COMMAND_TYPE.FORWARD; break; }
                case "BACKWARD": { commandType = COMMAND_TYPE.BACKWARD; break; }
                case "LEFT": { commandType = COMMAND_TYPE.LEFT; break; }
                case "RIGHT": { commandType = COMMAND_TYPE.RIGHT; break; }
                case "LAND": { commandType = COMMAND_TYPE.LAND; break; }                        
            }
            power = Integer.parseInt(lineTokens[1]);
            duration = Integer.parseInt(lineTokens[2]);
        }
    }
    
    public static Function<String, FlightCommand> mapToFlightCommand = (line) -> {
        return new FlightCommand(line);
    };    
    
}
